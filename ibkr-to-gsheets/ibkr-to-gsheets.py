from ibapi.client import EClient
from ibapi.wrapper import EWrapper
from ibapi.contract import Contract

import pygsheets
from pygsheets import Address
import threading
import time
import locale

asset_classes = {   'IAU': 'GOLD',
                    'VTI': 'TSM',
                    'VBR': 'SCV'}

class SheetClient():
    def __init__(self):

        self.sheet_id = '1CjG6t96iysUCRToL7THkFKJ2ToH6Dq2jkao02dYO4pw'
        self.worksheet = 'ФР'
        self.cell_value = 'E28'
        self.cell_cash = 'E3'
        self.cell_position_tickers = 'C4'
        self.cell_position_quantity = 'F4'
        self.cell_position_mean = 'G4'
        self.cell_position_class = 'N4'
        self.max_tickers = 21

        self.client = pygsheets.authorize(service_account_file='logger.json')
        self.sheet = self.client.open_by_key(self.sheet_id)
        self.wks = self.sheet.worksheet_by_title(self.worksheet)

        self.ticker_data = []

    def updateValue(self, value: float):
        self.wks.update_value(self.cell_value, value)

    def updateCash(self, value: float):
        self.wks.update_value(self.cell_cash, value)

    def clearPositions(self):
        for start_cell in [self.cell_position_tickers,
                           self.cell_position_quantity,
                           self.cell_position_mean,
                           self.cell_position_class]:
            start = Address(start_cell)
            end = start + (self.max_tickers-1, 0)
            self.wks.clear(start=start, end=end)

    def updatePositions(self, data):
        tickers = data['tickers']
        if len(tickers) > self.max_tickers:
            raise("Too many tickers")

        self.clearPositions()
        self.wks.update_values(self.cell_position_tickers, [data['tickers']], majordim='COLUMNS')
        self.wks.update_values(self.cell_position_quantity, [data['qty']], majordim='COLUMNS')
        self.wks.update_values(self.cell_position_mean, [data['avg']], majordim='COLUMNS')
        self.wks.update_values(self.cell_position_class, [data['class']], majordim='COLUMNS')


class IBapi(EWrapper, EClient):
    def __init__(self):
        EClient.__init__(self, self)
        self.sheet = SheetClient()

    def accountSummary(self, reqId, account, tag, value, currency):
        print(f'{tag} = {value}')

        if tag == 'NetLiquidation':
            self.sheet.updateValue(locale.atof(value))

        if tag == 'TotalCashValue':
            self.sheet.updateCash(locale.atof(value))

    def getPositions(self):
        self.ticker_data = {'tickers': [], 'qty': [], 'avg': [], 'class': []}
        self.reqPositions()

    def position(self, account: str, contract: Contract, position: float, avgCost: float):
        global asset_classes
        super().position(account, contract, position, avgCost)

        if contract.currency != 'USD' or contract.secType != 'STK':
            return

        if contract.symbol in asset_classes:
            asset_class = asset_classes[contract.symbol]
        else:
            asset_class = ''

        self.ticker_data['tickers'].append(contract.symbol)
        self.ticker_data['qty'].append(position)
        self.ticker_data['avg'].append(avgCost)
        self.ticker_data['class'].append(asset_class)

    def positionEnd(self):
        super().positionEnd()
        self.sheet.updatePositions(self.ticker_data)


app = IBapi()


def run_loop():
    app.run()


def try_sync():
    global app
    app.connect('127.0.0.1', 7496, 123)
    if not app.isConnected():
        return

    # Start the socket in a thread
    api_thread = threading.Thread(target=run_loop, daemon=True)
    api_thread.start()

    time.sleep(1)  # Sleep interval to allow time for connection to server

    # Request Market Data
    app.reqAccountSummary(9001, "All", "NetLiquidation,TotalCashValue")
    app.getPositions()

    time.sleep(20)  # Sleep interval to allow time for incoming price data
    app.disconnect()


try_sync()
