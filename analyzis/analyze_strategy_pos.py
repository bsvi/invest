# VYM
#growth_rate = 3.40/100
#dividend_rate = 3.12/100
#dividends_growth_rate = 6.4/100

# SPY
growth_rate = 7.27/100
dividend_rate = 1.81/100
dividends_growth_rate = 6/100

tax_rate = (18+1.5)/100
dividends_tax_rate = (15+1.5)/100
uah_inflation_rate = 11.0/100

initial_deposit_usd = 10000
annual_deposit_usd = 0

years = 20
usd_value = 1

class Position:
    def __init__(self, price_usd):
        self.value_usd = price_usd
        self.deposited_usd = price_usd
        self.deposited_uah = price_usd * usd_value
        self.dividends = 0
        self.years = 0


positions = []
def openPosition(price_usd):
    positions.append(Position(price_usd))


openPosition(initial_deposit_usd)

for y in range(1, years+1):
    usd_value = usd_value * (1+uah_inflation_rate)
    dividend_rate = dividend_rate * (1+dividends_growth_rate)

    total_dividends = 0
    taxes_usd = 0
    balance_usd = 0
    gain_value_product = 0
    for position in positions:
        position.years += 1

        dividends_usd = position.value_usd * dividend_rate * (1-dividends_tax_rate)
        position.dividends += dividends_usd
        position.value_usd += position.value_usd * growth_rate
        
        taxable_uah = position.value_usd * usd_value - position.deposited_uah
        tax_usd = taxable_uah * tax_rate / usd_value

        gain = (dividends_usd + position.value_usd - tax_usd - position.deposited_usd)/position.deposited_usd
        mean_gain = (gain+1)**(1/position.years)-1

        gain_value_product += mean_gain * position.value_usd
        balance_usd += position.value_usd
        taxes_usd += tax_usd
        total_dividends += dividends_usd

    mean_gain = gain_value_product / balance_usd

    print("Y: {0}, Bal: {1:.2f}, Tax: {2:.2f}, Out {3:.2f}, Gain {4:.2f}%".format(y, balance_usd, taxes_usd, balance_usd-taxes_usd, mean_gain*100) )

    freeMoney = total_dividends + annual_deposit_usd
    if freeMoney != 0:
        openPosition(freeMoney)
    