from ticker_data import getTickerData

ticker = "VIG"

def analyze(ticker, uah_inflation_percent, single_year=-1):
    print("Ticker {0}, UAH inflation {1:.2f}%".format(ticker, uah_inflation_percent))

    [growth_rate, dividend_rate, dividends_growth_rate] = getTickerData(ticker)
    tax_rate = (18+1.5)/100
    dividends_tax_rate = (15+1.5)/100
    uah_inflation_rate = uah_inflation_percent/100

    usd_value = 1
    balance_usd = 10000

    deposited_uah = balance_usd
    deposited_usd = balance_usd
    dividends_usd = 0

    years = 30

    for y in range(1, years+1):
        usd_value = usd_value * (1+uah_inflation_rate)

        # calculate dividends
        dividend_rate = dividend_rate * (1+dividends_growth_rate)
        dividends = balance_usd * dividend_rate
        dividends_tax = dividends * dividends_tax_rate
        
        # reinvest dividends
        dividends_total = dividends-dividends_tax
        dividends_usd += dividends_total
        balance_usd += dividends_total
        deposited_usd += dividends_total
        deposited_uah += dividends_total * usd_value

        # calculate growth
        value_increase = balance_usd * growth_rate
        balance_usd += value_increase

        taxable_uah = balance_usd*usd_value-deposited_uah
        tax_usd = (taxable_uah/usd_value)*tax_rate
        outcome_usd = balance_usd - tax_usd

        pure_money_deposited = deposited_usd - dividends_usd
        gain=(outcome_usd-pure_money_deposited)/pure_money_deposited

        if single_year <= 0 or y==single_year:
            print("Y: {0}, Bal: {1:.2f}, Tax: {2:.2f}, Out {3:.2f}, Gain {4:.2f}%".format(y, balance_usd, tax_usd, outcome_usd, gain*100) )

analyze("CSPX.L", 5, 21)
analyze("VYM", 5, 21)