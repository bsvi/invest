from yahoofinancials import YahooFinancials as YF
from datetime import datetime
from collections import OrderedDict
from dateutil.relativedelta import relativedelta
import pickle
import os.path


def formatDate(date):
    return date.strftime('%Y-%m-%d')

def GetData(ticker):
    if os.path.isfile(ticker+'.pkl'):
        with open(ticker+'.pkl', 'rb') as f:
            data = pickle.load(f)
        return data

    etf = YF(ticker)
    today = formatDate(datetime.now())
    data = etf.get_historical_price_data(today, today, 'daily')

    firstTradeDate = datetime.fromtimestamp(data[ticker]['firstTradeDate']['date'])
    firstTradeDateFormated = formatDate(firstTradeDate)
    data = etf.get_historical_price_data(firstTradeDateFormated, today, 'daily')

    data = data[ticker]
    with open(ticker+'.pkl', 'wb') as f:
        pickle.dump(data, f)
    return data



data = GetData('SPY')
drawdowns = []
drawdowns_count = 0
total_deals = 0

target_percents = 4.1
for i in range(2, len(data['prices'])-1):
    prev_open = data['prices'][i-1]['open']
    prev_close = data['prices'][i]['open']
    prev_drawdown = (prev_close-prev_open)/prev_open*100
    if prev_drawdown < -0.5:
        price_open = data['prices'][i]['open']
        price_close = data['prices'][i+1]['close']
        drawdown = (price_close - price_open)/price_open * 100
        if drawdown < -target_percents:
            drawdowns_count += 1

        if drawdown < 0:
            drawdowns.append(drawdown)
        total_deals += 1

drawdowns.sort()
print(f"Total loses {drawdowns_count} of {total_deals} deals")
print(f"Proboaility {(1-drawdowns_count / total_deals)*100}%")
print(drawdowns[:20])
