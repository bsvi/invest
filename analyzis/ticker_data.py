from yahoofinancials import YahooFinancials as YF
from datetime import datetime
from collections import OrderedDict
from dateutil.relativedelta import relativedelta
import pickle


def formatDate(date):
    return date.strftime('%Y-%m-%d')

def GetData(ticker):
    etf = YF(ticker)
    today = formatDate(datetime.now())
    data = etf.get_historical_price_data(today, today, 'monthly')

    firstTradeDate = datetime.fromtimestamp(data[ticker]['firstTradeDate']['date'])
    firstTradeDateFormated = formatDate(firstTradeDate)
    data = etf.get_historical_price_data(firstTradeDateFormated, today, 'monthly')

    data = data[ticker]
    with open('objs.pkl', 'wb') as f:
        pickle.dump(data, f)
    return data

def LoadData():
    with open('objs.pkl', 'rb') as f:
        data = pickle.load(f)
    return data


def getMeanDivs(dividend_data, dates):
    summ = 0
    for date in dates:
        summ += dividend_data[date]['amount']
    return summ/len(dates)

def getMeanPrice(prices):
    summ = 0
    for price in prices:
        summ += (price['high'] + price['low'])/2
    return summ/len(prices)

def getCagr(start_value, end_value, years):
    return (end_value/start_value)**(1/years)-1

def processData(data):
    firstTradeDate = datetime.fromtimestamp(data['firstTradeDate']['date'])
    years = relativedelta(datetime.now(), firstTradeDate).years + relativedelta(datetime.now(), firstTradeDate).months/12

    prices = data['prices']

    start_price = getMeanPrice(prices[:10])
    end_price = getMeanPrice(prices[-10:])

    price_growth = 100 * getCagr(start_price, end_price, years)

    if 'dividends' in data['eventsData']:
        dividends = data['eventsData']['dividends']
        div_dates = sorted(dividends.keys())

        start_divs = getMeanDivs(dividends, div_dates[:4])*4
        end_divs = getMeanDivs(dividends, div_dates[-4:])*4

        divs_percent = 100 * end_divs/end_price
        div_growth = 100 * getCagr(start_divs, end_divs, years)
    else:
        divs_percent = 0
        div_growth = 0

    print("Current divs {0:.2f}%, div increase {1:.2f}%, price growth {2:.2f}%".format(divs_percent, div_growth, price_growth))

    return [price_growth/100, divs_percent/100, div_growth/100]

def getTickerData(ticker):
    data = GetData(ticker)
    return processData(data)

#getTickerData("VOO")